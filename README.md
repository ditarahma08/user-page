# user-page

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3005
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

I just managed to finish the UI, I got CORS problem when trying to integrate it with the API. I alrady trying `mode: no-cors` but I can;t fetch the response because it's opaque type. I'm really sorry I can't finish to solve the problem due to the limited time.
